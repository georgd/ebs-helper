$(document).ready(function() {
  // create new project
  $('#sendNewProject').on('click', '#addEbsProject', function(e) {
    var form = $(this);
    var formdata = false;
    if (window.FormData){
      formdata = new FormData(form[0]);
    }

    $.ajax({
      url         : $(this).attr('action'),
      data        : formdata ? formdata : form.serialize(),
      cache       : false,
      contentType : false,
      processData : false,
      type        : $(this).attr('method'),
      success     : function(data, textStatus, jqXHR){
        console.log(data);
        $("#msg").html('Projekt erfolgreich angelegt');
        alert('Projekt erfolgreich angelegt')
      }
    });

    e.preventDefault();
  });

  var projectsTable = $('#tableProjects').DataTable({
      dom: '<"row" <"col-md-6" B><"dataTables_filter col-md-6" f>>rtip',
      buttons: [
        'pageLength',
        'excelHtml5',
        'csvHtml5',
        'colvis'
      ],
      ajax: {
        url: 'http://localhost:5000/api/v1/ebsProjects',
        dataSrc: 'items'
      },
      columns: [
        {
          className:      'details-control',
          orderable:      false,
          data:           null,
          defaultContent: ''
        },
        { data: "code" },
        { data: "name" },
        { data: "runtimeStart" },
        { data: "runtimeEnd"},
        { data: "timeCreated",
          render: function ( data, type, row, meta){
            if(data){
              return new Date(data*1000).toISOString().substring(0, 10);
            }
            return '–';
          } 
        },
        { data: "timeModified",
          render: function ( data, type, row, meta){
            if (data){
              return new Date(data*1000).toISOString().substring(0, 10);
            }
            return '–';
          },
        },
        { data: "active",
          className: "statusCell",
          render: function ( data, type, row, meta ){
            if (data){
              return '<input type="checkbox" class="editor-active" onclick="return false;" checked>';
            } else {
              return '<input type="checkbox" onclick="return false;" class="editor-active">';
            }
            return data;
          }
        },
        {
          className: 'actions',
          orderable: false,
          data: null,
          defaultContent: '',
          render: function (data, type, row, meta){
            return '<button type="button" class="btn proj-edit"><img src="images/iconmonstr-plus-4-16.png"/></button>' +
              '<button type="button" class="btn proj-delete" data-toggle="modal" data-projectcode="' + row['code'] + '" data-projecttitle="' + row['name'] + '" data-target="#deleteModal"><img src="images/iconmonstr-minus-4-16.png"/></button>'
          }
        }
      ]
    });

  // Add event listener for toggling activation status
  $('#tableProjects tbody').on('click', 'td.statusCell', function () {
    var tblData = projectsTable.row( $(this).parents('tr') ).data();
    var tr = $(this).closest('tr');
    var row = projectsTable.row( tr );
    var cell = projectsTable.cell(this);
    var ebsProject = tblData.code;

    jQuery.ajax({
      url: 'http://localhost:5000/api/v1/ebsProjects/'+ebsProject+'/toggleStatus',
      method: 'PUT',
      data: {status: !tblData.active},
      success: function(resp) {
        console.log(resp);
        if (resp["error"]) {
          $('#msg').addClass('alert alert-warning');
          $("#msg").html('Error: ' + resp["error"]);
        }
        if (resp["ok"]) {
          cell.data(resp["value"]).draw;
        }
      }
    });
  });

  // Add event listener for toggling activation status
  $('#tableProjects tbody').on('click', 'span.proj-delete', function () {
    var tblData = projectsTable.row( $(this).parents('tr') ).data();
    var tr = $(this).closest('tr');
    var row = projectsTable.row( tr );
    var cell = projectsTable.cell(this);
    var ebsProject = tblData.code;

  });

  $('#deleteModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var ebsProject = button.data('projectcode') // Extract info from data-* attributes
    var projTitle = button.data('projecttitle')
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.deleteModal-projectTitle').text(projTitle)
    modal.find('#deleteProject').attr('data-projectcode', ebsProject)
  })

  $('#deleteModal').on('click','#deleteProject', function(){
    var ebsProject = $(this).data('projectcode')
    jQuery.ajax({
      url: 'http://localhost:5000/api/v1/ebsProjects/'+ebsProject,
      method: 'DELETE',
      success: function(resp) {
        console.log(resp);
        $("#msg").html("Löschen von "+ resp['deleted_records'] + " erfolgreich.");
        if (resp["deleted_records"] == 1) {
          projectsTable.ajax.reload(null,false);
        }
      }
    });
  })
});
