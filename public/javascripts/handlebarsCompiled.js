(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['showPackageData'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <div class=\"card bg-light mb-3\">\n            <div class=\"card-body\">\n                <!--<div class=\"col-sm-2\"><input type=\"checkbox\" class=\"\" name=\"packageFile[]\" value=\""
    + alias4(((helper = (helper = helpers.marcFile || (depth0 != null ? depth0.marcFile : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"marcFile","hash":{},"data":data}) : helper)))
    + "\"/></div>-->\n                <!--<div class=\"col-sm-10\">-->\n                <div class=\"col-8\" style=\"float:left;\">\n                    <h5 class=\"card-title\">"
    + alias4(((helper = (helper = helpers.sigel || (depth0 != null ? depth0.sigel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sigel","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.packageMonth || (depth0 != null ? depth0.packageMonth : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"packageMonth","hash":{},"data":data}) : helper)))
    + "</h5>\n                    <div class=\"card-text\">\n                        <p>Originaldatei: "
    + alias4(((helper = (helper = helpers.originalFile || (depth0 != null ? depth0.originalFile : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"originalFile","hash":{},"data":data}) : helper)))
    + "</p>\n                        <p><em>DS in</em> "
    + alias4(((helper = (helper = helpers.recordsIn || (depth0 != null ? depth0.recordsIn : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"recordsIn","hash":{},"data":data}) : helper)))
    + "\n                            <em>DS out</em> "
    + alias4(((helper = (helper = helpers.recordsOut || (depth0 != null ? depth0.recordsOut : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"recordsOut","hash":{},"data":data}) : helper)))
    + "</p>\n                        <!--<a href=\"downloads/"
    + alias4(((helper = (helper = helpers.marcFile || (depth0 != null ? depth0.marcFile : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"marcFile","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn-primary\">Download</a>\n                        <button class=\"delete-packageFile btn btn-danger\" type=\"button\" data-id=\""
    + alias4(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">Löschen</button>-->\n                    </div>\n                </div>\n                <div class=\"col-4\" style=\"float:right;\">\n                        <a href=\"downloads/"
    + alias4(((helper = (helper = helpers.marcFile || (depth0 != null ? depth0.marcFile : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"marcFile","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn-primary mb-1\">Download</a>\n                        <button class=\"delete-packageFile btn btn-danger\" type=\"button\" data-id=\""
    + alias4(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">Löschen</button>\n                </div>\n            </div>\n        </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div>\n    <form action=\"http://localhost:5000/api/v1/catFiles\">\n        <!--<div><p><button id=\"catFiles\" type=\"submit\" class=\"btn btn-primary\">Dateien verketten</button></p></div>-->\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.items : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </form>\n</div>\n";
},"useData":true});
})();