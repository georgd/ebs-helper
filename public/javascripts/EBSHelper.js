var projectDetails;
function show_packageData(){
  $.get('http://localhost:5000/api/v1/packageFiles', {
    ebsProject : $("#ebsProject").val()
  }, function(data) {
    var html;
    html = '<table>';
    console.log(data);
    if (data["items"].length == 0){
      $("#msgPackageData").html('<span class="alert alert-info" role="alert">Noch keine Paketdaten vorhanden.</span>');
      $("#packageDataList").html();
    } else {
      // sort items in descending order (the API probably sends them in ascending order)
      data["items"].reverse();
      var templateScript = Handlebars.templates.showPackageData({ data: data });
      $("#packageDataList").html(templateScript);
      $(".delete-packageFile").click(delete_packageFile);
    }
  });
}

function delete_packageFile() {
  var id = $(this).attr('data-id');
  var ebsPackage = $("#ebsProject").val();
  jQuery.ajax({
    url: 'http://localhost:5000/api/v1/packageFiles/' + ebsPackage + '/' + id,
    type: 'DELETE',
    success: function(data) {
      console.log(data)
      show_packageData();
    }
  });
}

function buildUploadForm(formId,ebsProj) {
  if (formId.match(/MarcUploadForm/)){
    if (ebsProj.match(/VFV/g)){
      $('#paketSigel').attr({
        disabled: true,
      });
      $('#paketSigel-inputGroup').addClass('hidden');
      console.log(ebsProj + 'Vahlen Selected, xxx disabled');
    }
  }
}

function formatUsageDetails ( d ) {
  return '<div class="row">' + 
    '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    '<tr>'+
    '<td>ID</td>'+
    '<td>'+d.id+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>Print ISBN</td>'+
    '<td>'+d.pisbn+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>LJ-Nummer</td>'+
    '<td>'+d.localId+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>Autorinnen/Autoren</td>'+
    '<td>'+d.authors+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>Titel</td>'+
    '<td>'+d.title+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>DOI</td>'+
    '<td><a href="https://doi.org/'+d.doi+'" target="_blank">'+d.doi+'</a></td>'+
    '</tr>'+
    '<tr>'+
    '<td>Paketmonat</td>'+
    '<td>'+d.packageMonth+'</td>'+
    '</tr>'+
    '</table>' +
    '</div>';
}

function getProject(code){
  $.ajax({
    'url': 'http://localhost:5000/api/v1/ebsProjects/' + code, 
    'type': "GET",
    dataType: 'json'
  })
    .done (function(data, textStatus, jqXHR){
      projectDetails = data;
    });
}
function createdRow( row, data, dataIndex, cells ) {
  // Set a class
  if (data.purchaseProposal.hasOwnProperty('institut')){
    $(row).addClass('hasPurchaseRequest');
  } else if (data.purchaseProposal.hasOwnProperty('purchaseProposal')){
    $(row).addClass('hasProposal');
  }
}
function registerEventHandlers(dt, ebsProject){
  // Add event listener for opening and closing details
  $('#tableUsageData tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row =dt.row( tr );

    if ( row.child.isShown() ) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child( formatUsageDetails(row.data()) ).show();
      tr.addClass('shown');
    }
  });

  // Add event listener for adding and removing purchase proposals
  $('#tableUsageData tbody').on('click', 'td.purchaseProposal', function () {
    var tblData =dt.row( $(this).parents('tr') ).data();
    var tr = $(this).closest('tr');
    var row =dt.row( tr );
    var cell =dt.cell(this);

    if (row.node().className.match('hasProposal')){
      jQuery.ajax({
        url: 'http://localhost:5000/api/v1/purchaseProposal',
        method: 'DELETE',
        data: {ebsProject: ebsProject, id: tblData.id},
        success: function(data) {
          console.log(data);
          if (data["error"]) {
            $('#msg').addClass('alert alert-warning');
            $("#msg").html('Error: ' + data["error"]);
          }
          if (data["ok"]) {
            tr.removeClass('hasProposal');
            cell.data('').draw
          }
        }

      });
    } else if (!row.node().className.match('hasPurchaseRequest')){
      jQuery.post('http://localhost:5000/api/v1/purchaseProposal',
        {ebsProject: ebsProject, id: tblData.id},
        function(ajax_data) {
          console.log(ajax_data);
          if (ajax_data["error"]) {
            $('#msg').addClass('alert alert-warning');
            $("#msg").html('Error: ' + ajax_data["error"]);
          }
          if (ajax_data["ok"]) {
            tr.addClass('hasProposal');
            cell.data({purchaseProposal: ajax_data['name']}).draw;
          }
        });
    }
  });

}

function formatTitleCell(data, type, row){
  let returnStr = type === 'display' && data.length > 40 ?
    data.substr( 0, 38 )+'...' :
    data
  return '<a href="https://katalog.wu.ac.at/primo-explore/search?query=title,contains,'+
    encodeURI(data)+'&tab=wuw_alles&search_scope=WU-Bibliothekskatalog&vid=WUW" target="_blank" title="'+
    data +'">'+
    returnStr+'</a>'
}

function rowGroupRenderer(group, level, projectDetails, packages){
  if ( projectDetails.groups ){
    if ( level === 1 ){
      return packages[group]['short']+' ('+group+')'
    } else {
      return group
    }
  } else if ( level === 0 ){
    return packages[group]['short']+' ('+group+')'
  }
}

function renderPurchaseProposal(data){
  let returnString = '<span class="purchaseBtn"></span>';
  if (data.hasOwnProperty('institut')){
    returnString += '<span title="' + projectDetails.funds[data['institut']] 
      +'">'+data['institut']+'</span>';
  } else if (data.hasOwnProperty('purchaseProposal')){
    returnString += '<span>'+data['purchaseProposal']+'</span>';
  } else if (data.hasOwnProperty('print')){
    returnString += '<span>'+ data['print']+'</span>';
  }
  return returnString;
}

function formatAuthorCell(data, type){
  if (!data){
    return ""
  } else {
    return type === 'display' && data.length > 31 ?
      '<span title="'+data+'">'+data.substr( 0, 29 )+'...</span>' :
      data;
  }
}

function formatGroupCell(data, type){
  if (!data){
    return ""
  } else {
    return type === 'display' && data.length > 17 ?
      '<span title="'+data+'">'+data.substr( 0, 15 )+'...</span>' :
      data;
  }
}

function fDecimal(n,p=2){
  return  $.fn.dataTable.render.number('.', ',', p, '').display(n)
}

function findCol (columns, str){
  return columns.find(e=> e.data && e.data===str)
}
function filterCols (columns, str, method = 'match'){
  if (method ==='startsWith'){
    return columns.filter(x => x.data && x.data.startsWith(str))
  }
  return columns.filter(x => x.data && x.data.match(str))
}
function extendData(apiData, packages, tbl='usage'){
  var columns = apiData.columns
  $.extend(findCol(columns, 'usageFlag'), {
    className : 'usageFlag',

    searchPanes: {
      header: 'Nutzungsdaten',
      options: [
        {
          label: '<i>keine Nutzung</i>',
          value: function (rowData, rowIdx){
            return rowData.usageFlag !=='U'
          }
        },
        {
          label: 'Nutzungsdaten vorhanden',
          value: function (rowData, rowIdx){
            return rowData.usageFlag ==='U'
          }
        }
      ]
    }
  })
  $.extend(findCol(columns, 'purchaseProposal'), {
    render : {
      _: function ( data, type, row, meta ) {
      if (tbl ===  '#tableAcqRequests'){
        let returnString = '<span class="purchaseBtn"></span>';
        let returnData;
        if (data.hasOwnProperty('institut')){
          returnData = data['institut'];
          returnString += '<span class="sel" title="' + projectDetails.funds[returnData] 
            +'">'+returnData+'</span>';
        } else if (data.hasOwnProperty('purchaseProposal')){
          returnData = data['purchaseProposal'];
          returnString += '<span class="sel">'+ returnData +'</span>';
        } else if (data.hasOwnProperty('print')){
          returnData = data['print'];
          returnString += '<span class="sel">'+ returnData +'</span>';
        }
        return type === 'display' ? returnString :
          returnData;
      } else {
        return renderPurchaseProposal(data)
      }},
      sp: function( data ) {
        if (data.hasOwnProperty('institut')) {
          return data['institut']
        } else if (data.hasOwnProperty('purchaseProposal')){
          return data['purchaseProposal']
        } else if (data.hasOwnProperty('print')){
          return 'Print'
        }
        return ''
      }
    },
    searchPanes: {
      orthogonal: 'sp'
    },
    createdCell : function (td, cellData, rowData, row, col) {
      if (tbl ===  '#tableAcqRequests'){
        if (cellData.hasOwnProperty('print')) {
          $(td).addClass('hasPrintRequest');
        }
      }
    },
    className: "purchaseProposal"
  })
  $.extend(findCol(columns, 'isbn'), {
    searchPanes: {
      show: false,
    },
    className: "isbn"
  })
  $.extend(findCol(columns, 'authors'), {
    searchPanes: {
      show: false,
    },
    render: function ( data, type, row, meta ) {
      return formatAuthorCell(data, type)
    }
  })
  $.extend(findCol(columns, 'title'), {
    searchPanes: {
      show: false,
    },
    render: function ( data, type, row, meta ) {
      return formatTitleCell(data, type, row)
    }
  })
  $.extend(findCol(columns, 'year'), {
    searchPanes: {
      show: false,
    }
  })
  $.extend(findCol(columns, 'sigel'), {
    render: {
      _: function ( data, type, row, meta ) {
        return type === 'display' && packages[data] ?
          '<span title="'+packages[data].name+'">'+data+'</span>' :
          data;
      },
      sp: function( data ) {
        return data + ' (' + packages[data].name + ')'
      }
    },
    searchPanes: {
      orthogonal: 'sp'
    }
  })
  $.extend(findCol(columns, 'group'), {
    render: {
      _: function ( data, type, row, meta ){
        return formatGroupCell(data, type)
      },
      sp: function ( data ) {
        return data
      }
    },
    searchPanes: {
      orthogonal: 'sp'
    }
  })
  filterCols(columns,'usageTotal|activeMonths|usedMonths')
    .forEach(c => $.extend(c, {
      searchPanes: {
        show: false,
      },
      createdCell: function (td, cellData, rowData, row, col) {
        $(td).addClass('text-right');
      }
    })
    )
  filterCols(columns, 'usageRelative|usedMonthsRelative')
    .forEach(c => $.extend(c, {
      searchPanes: {
        show: false,
      },
      render: function ( data, type, row, meta ) {
        return fDecimal(data, 1);
      },
      createdCell: function (td, cellData, rowData, row, col) {
        $(td).addClass('text-right');
      }
    })
    )

  filterCols( columns,'price')
    .forEach(c => $.extend(c, {
      searchPanes: {
        show: false,
      },
      className: 'currency',
      render: function ( data, type, row, meta ) {
        return fDecimal(data);
      },
      createdCell: function (td, cellData, rowData, row, col) {
        if (cellData !== null) {
          $(td).addClass(rowData['currency']);
        }
      }
    }))
  filterCols( columns,'prices.','startsWith')
    .forEach(c => $.extend(c, {
      searchPanes: {
        show: false,
      },
      className: 'currency',
      render: function ( data, type, row, meta ) {
        return fDecimal(data);
      },
      createdCell: function (td, cellData, rowData, row, col) {
        if (cellData !== null) {
          $(td).addClass(rowData['currency']);
        }
      }
    }))
  filterCols( columns, 'usageData.', 'startsWith')
    .forEach(c => $.extend(c, {
      searchPanes: {
        show: false,
      },
      defaultContent: '',
      render: function ( data, type, row, meta ) {
        if (data === 'NA'){
          return '—'
        }
        return data;
      },
      createdCell: function (td, cellData, rowData, row, col) {
        $(td).addClass('text-right');
      }
    }))
  $.extend(findCol(columns, 'pisbn'), {
    searchPanes: {
      show: false,
    },
    visible: false
  })

  $.extend(findCol(columns, 'license'), {
    searchPanes: {
      show: false,
    },
    render: function ( data, type, row, meta ) {
      if (type === 'display' && data.length > 0){
        var $select = $("<select></select>", {
          "id": row[0]+"license",
          "value": data,
          "class": "custom-select license-select"
        });
        var licenses = apiData.licenses;
        licenses.forEach( function(v){
          var $option = $("<option></option>", {
            "text": v,
            "value": v
          });
          if(data === v){
            $option.attr("selected", "selected")
          }
          $select.append($option);
        });
        return $select.prop("outerHTML");
      }
      return data;
    }
  })
}

function extendOverviewData(apiData, packages, contractualCurrency){
  var columns = apiData.columns
  $.extend(findCol(columns.packages, 'pkgName'), {
    render: function ( data, type, row, meta ) {
      if (type === 'display' && packages[data] ){
        let pkgName = packages[data].name
        pkgName = pkgName.replace(/.+?\s+\/\s+/, '')
        return '<span title="'+data+'">'+pkgName+'</span>'
      }
      return data
    }
  })
  $.extend(findCol(columns.packages, 'group'), {
    visible: false
  })
  $.extend(findCol(columns.titles, 'size'), {
    className: 'text-right sum'
  })
  columns.acqRequests.forEach(c => $.extend(c, {
    defaultContent: '',
    className: c.data.match('.*Price') ? 'text-right currency sum ' + contractualCurrency : 'text-right sum',
    render: function ( data, type, row, meta ){
      if (c.data.match('.*Price')){
        return fDecimal(data)
      }
      return data
    }
  }))
  columns.usage.forEach(c => $.extend(c, {
    defaultContent: '',
    className: 'text-right sum'
  }))
  $.extend(columns.usageTitles.total, {
    defaultContent: '',
    className: 'text-right sum'
  })
  columns.usageTitles.forEach(c => $.extend(c, {
    defaultContent: '',
    className: 'text-right sum'
  }))
}

function sumColumn( column ){
  return column.data()
    .reduce(function(a, b) {
      var x = parseFloat(a) || 0;
      var y = parseFloat(b) || 0;
      return x + y;
    }, 0)
}

$(document).ready(function() {
  $('#ebsProject').on('change', function(){ window.location.reload()});
  var ebsProject = $("#ebsProject").val();

  $.ajax({
    'url': 'http://localhost:5000/api/v1/ebsProjects/' + ebsProject, 
    'type': "GET",
    dataType: 'json'
  }).done (function(data, textStatus, jqXHR){
    projectDetails = data;
    if (projectDetails['active']){
      $('body').removeClass('inactive');
    } else {
      $('body').addClass('inactive');
    }

    var packages = new Object;
    projectDetails.packages.forEach(e => {
      let s = e.sigel;
      packages[s] = new Object;
      packages[s].name = e.name;
      if (e['short']){
        packages[s]['short'] = e['short'];
      } else {
        packages[s]['short'] = e.name;
      }
    })

    $('#inputFile').on('change', function() {
      // get the file name
      var fileName = $(this).val();
      // replace the "Choose a file" label
      $(this).next('.custom-file-label').html(fileName);
    });

    var marcUploadForm = $('#MarcUploadForm')
    if (marcUploadForm.length) {
      buildUploadForm('#MarcUploadForm', ebsProject);
      show_packageData();
      $("#MarcUploadForm").submit(function(e) {
        e.preventDefault()
        var form = $(this);
        var formdata = false;
        if (window.FormData){
          formdata = new FormData(form[0]);
        }

        $.ajax({
          url         : $(this).attr('action') + '/' + ebsProject,
          data        : formdata ? formdata : form.serialize(),
          cache       : false,
          contentType : false,
          processData : false,
          type        : $(this).attr('method'),
          success     : function(data, textStatus, jqXHR){
            console.log(data);
            if (data["error"]) {
              $('#msg').addClass('alert alert-warning');
              $("#msg").html('Error: ' + data["error"]);
            }
            if (data["ok"]) {
              $("#msg").addClass("alert alert-success");
              $("#msg").html('Sigel ' + data["sigel"] + ': ' + data["num"] +
                ' records added');
            }
            show_packageData();
          }
        });
        return false;
      });
    }

    if( $('#projectOverview').length ){
      let contractualCurrency = projectDetails['contractualSum']['currency']
      let contractualSum = projectDetails['contractualSum']['sum']
      $('#projectTitle').html( projectDetails['name'])
      $('#runtime').append('<span>' + projectDetails['runtimeStart'] + ' – ' +
        projectDetails['runtimeEnd'] + '</span>')
      $('#totalSum').append('<span>' + contractualCurrency + ' ' +
        fDecimal(contractualSum) + '</span>')
      $.ajax({
        type: 'GET',
        url: 'http://localhost:5000/api/v1/ebsProjects/' + ebsProject + '/statistics',
        success: function(apiData){

          var allocatedSum = 0

          extendOverviewData(apiData, packages, contractualCurrency)
          let packageColumns = apiData.columns.packages
          let titlesColumns = apiData.columns.titles
          let acqReqColumns = apiData.columns.acqRequests
          let usageColumns = apiData.columns.usage
          let usageTitlesColumns = apiData.columns.usageTitles

          var tableName = '#titleCountTable'
          var columnDefs = packageColumns.concat(titlesColumns)
          $.each(columnDefs, function (k, colObj) {
            str = '<th title="' + colObj.tooltip + '">' + colObj.name + '</th>'
            $(str).appendTo(tableName+'>thead>tr');
            fStr = '<th></th>'
            $(fStr).appendTo(tableName+'>tfoot>tr');
          });

          var totalTitles
          var titleCountTable = $(tableName).DataTable({
            dom: 't',
            data: apiData['items'],
            pageLength: 100,
            footer: true,
            columns: columnDefs,
            order: [[0, 'asc']],
            footerCallback: function( row, data, start, end, display ){
              let invisibleCols = 0
              this.api().columns().every(function (i) {
                let column = this
                let sum = ''
                if ( $(column.header()).hasClass("sum") ) {
                  sum = sumColumn(column)
                  totalTitles = sum
                }
                if (column.visible() === true){
                  $(tableName + ' tfoot tr th:nth-child('+ (i+1-invisibleCols) +')').html(sum)
                } else {
                  invisibleCols++
                }
              })
              $('#totalTitles').append(totalTitles);
            }
          });

          tableName = '#packageAcqTable'
          columnDefs = packageColumns.concat(acqReqColumns)
          $.each(columnDefs, function (k, colObj) {
            str = '<th title="' + colObj.tooltip + '">' + colObj.name + '</th>'
            $(str).appendTo(tableName+'>thead>tr');
            fStr = '<th></th>'
            $(fStr).appendTo(tableName+'>tfoot>tr');
          });
          var packageAcqTable = $(tableName).DataTable({
            dom: 't',//'<"row" <"col-md-8" B><"dataTables_filter col-md-4" f>>rtip',
            pageLength: 100,
            lengthChange: false,
            data: apiData['items'],
            footer: true,
            columns: columnDefs,
            order: [[0, 'asc']],
            footerCallback: function( row, data, start, end, display ){
              let invisibleCols = 0
              this.api().columns().every(function (i) {
                let column = this;
                let sum = ''
                if ( $(column.header()).hasClass("sum") ) {
                  sum = sumColumn(column)
                }
                if ( $(column.header()).hasClass('currency')){
                  allocatedSum += sum
                  sum = fDecimal(sum)
                }
                if (column.visible() === true){
                  $(tableName + ' tfoot tr th:nth-child('+ (i+1-invisibleCols) +')').html(sum)
                } else {
                  invisibleCols++
                }
              });
              $('#allocatedSum').append(contractualCurrency + " "+fDecimal(allocatedSum));
              $('#rest').append(contractualCurrency + " "+fDecimal(contractualSum - allocatedSum));
            }
          });

          tableName = '#packageUsageTable'

          columnDefs = packageColumns.concat(usageColumns)
          columnDefs[1].visible = true

          $.each(columnDefs, function (k, colObj) {
            str = '<th title="' + colObj.tooltip + '">' + colObj.name + '</th>'
            $(str).appendTo(tableName+'>thead>tr');
            fStr = '<th></th>'
            $(fStr).appendTo(tableName+'>tfoot>tr');
          });

          var packageUsageTable = $(tableName).DataTable({
            dom: 't',//'<"row" <"col-md-8" B><"dataTables_filter col-md-4" f>>rtip',
            pageLength: 100,
            lengthChange: false,
            data: apiData['items'],
            footer: true,
            columns: columnDefs,
            order: [[0, 'asc']],
            footerCallback: function( row, data, start, end, display ){
              let invisibleCols = 0;
              this.api().columns().every(function (i) {
                let column = this
                let sum = ''
                if ( $(column.header()).hasClass("sum") ) {
                  sum = sumColumn(column)
                }
                if (column.visible() === true){
                  $(tableName + ' tfoot tr th:nth-child('+ (i+1-invisibleCols) +')').html(sum)
                } else {
                  invisibleCols++
                }
              })
            }
          });

          tableName = '#packageUsageTitlesTable'
          relativeSizeCol = {
            data: 'size',
            className: 'text-right',
            name: '%',
            render: function ( data, type, row, meta ){
              if (type === 'display'){
                let percentage = fDecimal(row.usageTitles['total']/data * 100);
                return percentage + ' %';
              }
              return data;
            }
          }

          columnDefs = packageColumns
            .concat(usageTitlesColumns)
            .concat(relativeSizeCol)

          columnDefs[1].visible = true

          $.each(columnDefs, function (k, colObj) {
            str = '<th title="' + colObj.tooltip + '">' + colObj.name + '</th>'
            $(str).appendTo(tableName+'>thead>tr');
            fStr = '<th></th>'
            $(fStr).appendTo(tableName+'>tfoot>tr');
          });

          var packageUsageTitlesTable = $(tableName).DataTable({
            dom: 't',//'<"row" <"col-md-8" B><"dataTables_filter col-md-4" f>>rtip',
            pageLength: 100,
            lengthChange: false,
            data: apiData['items'],
            columns: columnDefs,
            order: [[0, 'asc']],
            columnDefs: [
              {
                targets: 1,
                data: 'group',
                visible: true
              }
            ],
            footerCallback: function( row, data, start, end, display ){
              let columns = this.api().columns()
              let invisibleCols = 0;
              let totalColIdx = columns[0].length - 2
              let totalUsedTitles
              let totalUsedTitlesPercent
              let percColIdx = columns[0].length - 1
              this.api().columns().every(function (i) {
                let column = this
                let sum = ""
                if ( $(column.header()).hasClass("sum") ) {
                  sum = sumColumn(column)
                  if (i === totalColIdx) {
                    totalUsedTitles = sum
                  }
                } else if (i === percColIdx) {
                  sum = fDecimal(totalUsedTitles / totalTitles * 100) + ' %'
                  totalUsedTitlesPercent = sum
                }
                if (column.visible() === true){
                  $(tableName + ' tfoot tr th:nth-child('+ (i+1-invisibleCols) +')').html(sum)
                } else {
                  invisibleCols++
                }
              })
              $('#totalUsedTitles').append(totalUsedTitles + ' (' + totalUsedTitlesPercent + ')' )
            }
          });
        }
      })
    }

    if($('#postPurchaseProposal').length){
      $("#submitPurchaseProposal").click(function(){
        var isbn = $("#isbn").val();
        jQuery.post('http://localhost:5000/api/v1/purchaseProposal',
          {ebsProject:ebsProject,isbn:$("#isbn").val()},
          function(data) {
            console.log(data);
            if (data["error"]) {
              $('#msg').removeClass('alert-success alert-warning');
              $('#msg').addClass('alert alert-danger');
              $("#msg").html("Fehler beim Hinzufügen von  "+ isbn + " – " + data["error"]);
            } else if (data["warning"]) {
              $('#msg').removeClass('alert-success alert-danger');
              $('#msg').addClass('alert alert-warning');
              $("#msg").html("Fehler beim Hinzufügen von  "+ isbn + " – " + data["warning"]);
            } else if (data["ok"]) {
              $('#msg').removeClass('alert-warning alert-danger');
              $("#msg").addClass("alert alert-success");
              $("#msg").html(isbn + ' wurde hinzugefügt');
              acqRequestsTable.ajax.reload(null,false);
            } else {
              $('#msg').removeClass('alert-success alert-warning');
              $('#msg').addClass('alert alert-danger');
              $("#msg").html("Fehler beim Hinzufügen von  "+ isbn);
            }
          });
        return false;
      });
    }

    var detailsControl = {
      name: '',
      className: 'details-control',
      orderable: false,
      data: null,
      defaultContent: ''
    }
    var dtLangSettings = {
      searchPanes: {
        clearMessage: 'Auswahl zurücksetzen',
        collapse: {0: 'Filter', _: 'Aktive Filter (%d)'},
        title: {
          _: 'Filter ausgewählt - %d',
          0: 'Keine Filter ausgewählt',
          1: 'Ein Filter ausgewählt',
        }
      },
      decimal: ",",
      thousands: ".",
      url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/German.json"
    }

    if (document.getElementById('tableAcqRequests')){
      var printOrdersFlag = true;
      var apiData,
        tableName= '#tableAcqRequests',
        columns,
        str,
        jqxhr = $.ajax('http://localhost:5000/api/v1/acqRequests?ebsProject=' + ebsProject)
        .done(function () {
          apiData = JSON.parse(jqxhr.responseText);
          columns = apiData.columns
          columns.unshift(detailsControl)

          // Iterate each column and print table headers for Datatables
          $.each(apiData.columns, function (k, colObj) {
            console.log(colObj)
            str = '<th title="' + colObj.tooltip + '">' + colObj.name + '</th>'
            $(str).appendTo(tableName+'>thead>tr')
          })

          extendData(apiData, packages, tableName)

          filterCols( apiData.columns, 'prices.', 'startsWith')
            .forEach(c => c.visible = false)

          var exportOptions = {
            orthogonal: 'export',  
            columns : Array.from({length: apiData.columns.length-1},(v,k)=>k+1)
          }
          var acqRequestsTable = $(tableName).DataTable({
            language: dtLangSettings,
            dom: '<"row" <"col-md-8" B><"dataTables_filter col-md-4" f>>rtip',
            lengthChange: false,
            fixedHeader: true,
            pageLength: 200,
            lengthMenu: [ 50, 100, 200 ],
            buttons: [
              'pageLength',
              {
                extend: 'copyHtml5',
                exportOptions: exportOptions
              },
              {
                extend: 'excelHtml5',
                exportOptions: exportOptions
              },
              {
                extend: 'csvHtml5',
                exportOptions: exportOptions
              },
              {
                extend: 'pdfHtml5',
                exportOptions: exportOptions,
                orientation: 'landscape'
              },
              'colvis',
              {
                text: 'Printbestellungen aus',
                attr: {
                  id: 'printToggle',
                  title: 'Printbestellungen ein- oder ausblenden',
                  'data-toggle': 'button'
                },
                action: function (e, dt, node, config){
                  if (printOrdersFlag == true ){
                    acqRequestsTable.columns(5)
                      .search('^(?:(?!PRINT).)*$\r?\n?', true, false)
                      .draw();
                    printOrdersFlag = false;
                    node.text("Printbestellungen ein")
                  } else {
                    acqRequestsTable.columns(5)
                      .search('')
                      .draw();
                    printOrdersFlag = true;
                    node.text("Printbestellungen aus")
                  }
                }
              },
              {
                text: 'Nach ReferentInnen gruppieren',
                attr: {
                  id: 'selToggle',
                  title: 'Nach ReferentInnen gruppieren',
                  'data-toggle': 'button'
                },
                action: function ( e, dt, node, config ) {
                  if (rowGroupFlag == false ) {
                    acqRequestsTable.rowGroup().enable().draw();
                    rowGroupFlag = true;
                  } else {
                    acqRequestsTable.rowGroup().disable().draw();
                    rowGroupFlag = false;
                  }
                }
              }
            ],
            data: apiData.items,
            columns: columns,
            order: [[5, 'asc']],
            rowGroup: {
              enable : false,
              startRender: function( rows, group ) {
                var groupRow = $('<tr/>')
                  .append( '<td colspan="5" class="text-right">'+group+'</td>' )
                  .append( '<td >'+group+'</td>' )
                  .append( '<td colspan="3"></td>' );

                return groupRow;
              },
              dataSrc: function(row){
                if (row.purchaseProposal['purchaseProposal'] !== null){
                  console.log(row.purchaseProposal['institut'])
                  return row.purchaseProposal['purchaseProposal']
                } else if (row.purchaseProposal['institut'] !== null){
                  return row.purchaseProposal['institut']
                } else {
                  return 'PRINT'
                }
              },
              emptyDataGroup: 'Sonstige'
            },
            createdRow: function(row, data, dataIndex){
              createdRow( row, data, dataIndex )
            },
            initComplete: function ( settings, apiData ){
              afterAcqReqTblInit(settings, apiData);
            }
          });
          function afterAcqReqTblInit( settings, json ){
            $('.license-select').on('change',  function (e) {
              var tblData = acqRequestsTable.row( $(this).parents('tr') ).data();
              jQuery.ajax({
                url: 'api/v1/concurrentUsers',
                method: 'POST',
                data: {ebsProject: ebsProject, id: tblData.id, license: $(this).val()},
                success: function(data) {
                  if (data["error"]) {
                    $('#msg').removeClass('alert-success alert-warning')
                    $('#msg').addClass('alert alert-danger')
                    $("#msg").html("Fehler beim Lizenz ändern  – " + data["error"])
                  } else if (data["warning"]) {
                    $('#msg').removeClass('alert-success alert-danger')
                    $('#msg').addClass('alert alert-warning')
                    $("#msg").html("Fehler beim – " + data["warning"])
                  } else if (data["ok"]) {
                    $('#msg').removeClass('alert-warning alert-danger')
                    $("#msg").addClass("alert alert-success")
                    $("#msg").html('Lizenz geändert')
                    acqRequestsTable.ajax.reload(afterAcqReqTblInit, false)
                  }
                }
              })
            })

            // Add event listener for adding and removing purchase proposals
            $('#tableAcqRequests tbody')
              .on('click', 'td.purchaseProposal', function () {
                var tblData = acqRequestsTable.row( $(this).parents('tr') ).data();
                var tr = $(this).closest('tr');
                var row = acqRequestsTable.row( tr );
                var cell = acqRequestsTable.cell(this);
                console.log(row.node().className)
                console.log(cell.node().className)

                if (row.node().className.match('hasProposal')
                  || cell.node().className.match('hasPrintRequest')){
                  var strMethod;
                  var clTableColor;
                  var strDeleting;
                  var strDeleted;
                  if (row.node().className.match('table-danger')
                    || !row.node().className.match('hasProposal')){
                    method = 'POST';
                    clTableColor = 'table-success';
                    clOldTableColor = 'table-danger';
                    strErrorOnAction = 'Fehler beim Hinzufügen – ';
                    strDone = 'Ankaufswunsch hinzugefügt';
                  } else {
                    method = 'DELETE';
                    clTableColor = 'table-danger';
                    clOldTableColor = 'table-success';
                    strErrorOnAction = 'Fehler beim Entfernen – ';
                    strDone = 'Ankaufswunsch entfernt';
                  }
                  jQuery.ajax({
                    url: 'http://localhost:5000/api/v1/purchaseProposal',
                    method: method,
                    data: {ebsProject: ebsProject, id: tblData.id},
                    success: function(data) {
                      console.log(data);
                      if (data["error"]) {
                        $('#msg').removeClass('alert-success alert-warning');
                        $('#msg').addClass('alert alert-danger');
                        $("#msg").html(strErrorOnAction + data["error"]);
                      } else if (data["warning"]) {
                        $('#msg').removeClass('alert-success alert-danger');
                        $('#msg').addClass('alert alert-warning');
                        $("#msg").html(strErrorOnAction + data["warning"]);
                      } else if (data["ok"]) {
                        $('#msg').removeClass('alert-warning alert-danger');
                        $("#msg").addClass("alert alert-success");
                        $("#msg").html(strDone);
                        tr.removeClass(clOldTableColor);
                        tr.addClass(clTableColor);
                      }
                    }

                  });
                }
              })

            // Add event listener for adding ACQ statistics
            var contractCurrency = projectDetails.contractualSum.currency;
            var contractualSum = projectDetails.contractualSum.sum;
            var priceListCurrency = apiData.total.currency;
            var allocatedSum = apiData.total.totalLicenses;
            var rest;
            var selectors = apiData.selectors;
            var userAttribute = apiData.user_attribute;

            $("#totalSum").html(contractCurrency+ ' ' +fDecimal(contractualSum));
            $("#allocatedSum").html(priceListCurrency+ ' ' +fDecimal(allocatedSum));

            if (contractCurrency !== priceListCurrency) {
              var convertedAllocation = Math.round(allocatedSum * apiData.conversionTable[priceListCurrency+'_'+contractCurrency] * 100) / 100;
              $("#allocatedSum").append(
                ' (' +contractCurrency+ ' ' +fDecimal(convertedAllocation)+')'
              );
              rest = contractualSum-convertedAllocation;
            } else { 
              rest = contractualSum-allocatedSum
            }
            $("#rest").html(contractCurrency+ ' ' +fDecimal(rest));

            $("#ref-wuensche").empty();
            Object.keys(selectors).sort().forEach((key)=>{
              if (key === userAttribute){
                var selClassName = "selector selector-active";
              } else {
                selClassName = "selector";
              }
              $("#ref-wuensche").append('<div class="d-flex justify-content-between '+
                selClassName + '"><span>' + key +
                '</span><span title="Ausgewählte Bücher">'+ selectors[key]['books'] +
                '</span><span class="currency ' + priceListCurrency + '">' + 
                fDecimal(selectors[key]['price']) + '</span></div>');
            })

            // Add event listener for opening and closing details
            $('#tableAcqRequests tbody')
              .on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = acqRequestsTable.row( tr );

                if ( row.child.isShown() ) {
                  // This row is already open - close it
                  row.child.hide();
                  tr.removeClass('shown');
                }
                else {
                  // Open this row
                  row.child( formatUsageDetails(row.data()) ).show();
                  tr.addClass('shown');
                }
              });
          }
        })
    }

    // code for /price-list
    var priceListUploadForm = $('#priceListUploadForm');
    if (priceListUploadForm.length) {

      // upload price list
      $(document).on('submit', '#priceListUploadForm', function(e) {
        var form = $(this);
        var formdata = false;
        if (window.FormData){
          formdata = new FormData(form[0]);
        }

        $.ajax({
          url         : $(this).attr('action') + '/' + ebsProject,
          data        : formdata ? formdata : form.serialize(),
          cache       : false,
          contentType : false,
          processData : false,
          type        : $(this).attr('method'),
          success     : function(data, textStatus, jqXHR){
            console.log(data);
            $("#msg").html('Pricelist uploaded successfully');
            if (data["noPriceCount"]) {
              $("#msg").html(data["noPriceCount"] + ' Datensätze ohne Preisangabe');
            }
          },
          error       : function(){
            alert('Error in form submission!');
          }
        });

        e.preventDefault();
      });
    }

    var rowGroupFlag = false;
    var usageDataTable;
    var searchPanesDef = {
      cascadePanes : true,
      viewTotal: true,
      layout: 'columns-4',
      emptyMessage: '<i>leer</i>',
      dtOpts:{
        dom:"tp",
        paging: true,
        pagingType: 'numbers',
      }
    }
    var buttonList = 
      [
        'pageLength',
        //'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        //'pdfHtml5',
        'colvis',
        {
          extend: 'searchPanes',
          text: 'Filter',
          config: searchPanesDef
        },
        {
          text: 'Nach Paketen gruppieren',
          attr: {
            id: 'pkgToggle',
            title: 'Nach Paketen gruppieren',
            'data-toggle': 'button'
          },
          action: function ( e, dt, node, config ) {
            if (rowGroupFlag == false ) {
              usageDataTable.rowGroup().enable().draw();
              rowGroupFlag = true;
            } else {
              usageDataTable.rowGroup().disable().draw();
              rowGroupFlag = false;
            }
          }
        }
      ]
    if (document.getElementById("usageData")){
      var apiData,
        tableName= '#tableUsageData',
        columns,
        str,
        jqxhr = $.ajax('http://localhost:5000/api/v1/usage?ebsProject=' + ebsProject)
        .done(function () {
          apiData = JSON.parse(jqxhr.responseText);
          columns = apiData.columns
          columns.unshift(detailsControl)

          var colList = [];
          for (i = 0; i < apiData.columns.length; i++ ){
            if (apiData.columns[i].data 
              && apiData.columns[i].data.match(/usageTotal|usageData/)){
              colList.push(i) 
            }
          }

          // Iterate each column and print table headers for Datatables
          $.each(apiData.columns, function (k, colObj) {
            console.log(colObj)
            str = '<th title="' + colObj.tooltip + '">' + colObj.name + '</th>'
            $(str).appendTo(tableName+'>thead>tr')
            if (colList.find(e=>e===k)){
              $('<th class="text-right"></th>').appendTo(tableName+'>tfoot>tr')
            } else {
              $('<th></th>').appendTo(tableName+'>tfoot>tr')
            }
          })

          extendData(apiData, packages)

          usageDataTable = $(tableName).DataTable({
            language: dtLangSettings, 
            dom: '<"row" <"col-md-6" B><"dataTables_filter col-md-6" f>>rtip',
            fixedHeader: {
              header: true,
              footer: false
            },
            lengthChange: false,
            pageLength: 1000,
            lengthMenu: [ 20, 50, 100, 300, 500, 1000 ],
            scrollX: true,
            scrollY: true,
            buttons: buttonList,
            data: apiData.items,
            columns: columns,
            order: [[6, 'asc']],
            rowGroup: {
              enable : false,
              startRender: function( rows, group, level ) {
                var usageTotal = rows
                  .data()
                  .pluck('usageTotal')
                  .reduce( function (a, b) {
                    return a + b*1;
                  }, 0)

                var usageRelative = rows
                  .data()
                  .pluck('usageRelative')
                  .reduce( function (a, b) {
                    return a + b*1;
                  }, 0)

                usageRelative = $.fn.dataTable.render.number('.', ',', 1, '').display( usageRelative );

                var restCols = apiData.columns.length - apiData.columns.findIndex(e=>e.data==='activeMonths')
                var groupString = rowGroupRenderer(group, level, projectDetails, packages);
                return $('<tr/>')
                  .append( '<td/>' )
                  .append( '<td colspan="7">'+groupString+'</td>' )
                  .append( '<td class="text-right">'+usageTotal+'</td>' )
                  .append( '<td class="text-right">'+usageRelative+'</td>' )
                  .append( '<td colspan="'+restCols+'"></td>' );
              },
              dataSrc: projectDetails.groups  ? ['group','sigel'] : 'sigel'
            },
            createdRow: function( row, data, dataIndex ) {
              createdRow( row, data, dataIndex )
            },
            footerCallback: function( row, data, start, end, display ){

              let api = this.api();
              var intVal = function ( i ) {
                return typeof i === 'string' ?
                  //i.replace(/[\$,]/g, '')*1 :
                  0 :
                  typeof i === 'number' ?
                  i : 0;
              };

              for (i=0; i < colList.length; i++) {
                var index = colList[i];

                // Total over this page
                pageTotal = api
                  .column( index, { page: 'current'} )
                  .data()
                  .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                  }, 0 );

                // Update footer
                $( api.column( index ).footer() ).html(
                  pageTotal
                );
              }
            },
            initComplete: function(settings, json){

              $('.dataTables_scrollBody').on('scroll',function(){
                $('.dataTables_scrollHeadInner').scrollLeft($(this).scrollLeft());
              });

              $(document).on('scroll',function(){

                var scroll_pos = $(this).scrollTop();
                //var margin = 74; // Adjust it to your needs
                var margin = 100;
                var cur_pos = $('.dataTables_scrollHeadInner').position();
                var header_pos = cur_pos.top;

                if(scroll_pos < margin)
                  var header_pos = margin - scroll_pos;
                else
                  header_pos = 0;

                $('.dataTables_scrollHeadInner').css({"top" : header_pos})
              })
            }
          })
          registerEventHandlers(usageDataTable, ebsProject)
        })
    } else if (document.getElementById('allTitles')){

      var apiData,
        tableName= '#tableUsageData',
        columns,
        str,
        jqxhr = $.ajax('http://localhost:5000/api/v1/titles?ebsProject=' + ebsProject)
        .done(function () {
          apiData = JSON.parse(jqxhr.responseText);
          columns = apiData.columns
          columns.unshift(detailsControl) 

          // Iterate each column and print table headers for Datatables
          $.each(apiData.columns, function (k, colObj) {
            str = '<th>' + colObj.name + '</th>';
            $(str).appendTo(tableName+'>thead>tr');
          });

          extendData(apiData, packages)

          console.log(ebsProject)

          usageDataTable = $(tableName).DataTable({
            language: dtLangSettings,
            dom: '<"row" <"col-md-6" B><"dataTables_filter col-md-6" f>>rtip',
            fixedHeader: {
              header: true,
              footer: false
            },
            lengthChange: false,
            pageLength: 100,
            lengthMenu: [ 20, 50, 100, 300, 500, 1000 ],
            scrollX: true,
            //scrollY: true,
            buttons: buttonList,
            data: apiData.items,
            columns: columns,
            fnInitComplete: function () {
              // Event handler to be fired when rendering is complete (Turn off Loading gif for example)
              console.log('Datatable rendering complete');
            },

            order: [[7, 'asc']],
            rowGroup: {
              enable : false,
              startRender: function( rows, group, level ){
                return rowGroupRenderer(group, level, projectDetails, packages)
              },
              dataSrc: projectDetails.groups  ? ['group','sigel'] : 'sigel'
            },
            createdRow: function(row, data, dataIndex){
              createdRow( row, data, dataIndex )
            }
          })
          registerEventHandlers(usageDataTable, ebsProject)
        })
    }
  });
});

