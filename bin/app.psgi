#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";


=begin comment
# use this block if you don't need middleware, and only have a single target Dancer app to run here
use EBSHelper;

EBSHelper->to_app;

=end comment

=cut

# use this block if you want to include middleware such as Plack::Middleware::Deflater

use EBSHelper;
use Plack::Builder;

builder {
    enable 'Plack::Middleware::Static',
        path => qr{^/(javascripts|css)/},
        root => './public/';
    enable 'Deflater';
    EBSHelper->to_app;
};

=begin comment
# use this block if you want to mount several applications on different path

use EBSHelper;
use EBSHelper_admin;

use Plack::Builder;

builder {
    mount '/'      => EBSHelper->to_app;
    mount '/admin'      => EBSHelper_admin->to_app;
}

=end comment

=cut

