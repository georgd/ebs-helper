package EBSHelper;

use v5.16;
use Dancer2;
use Dancer2::Plugin::Auth::Extensible;
use POSIX qw(strftime);

#use utf8::all;
use JSON::MaybeXS qw//;
use List::Util qw(first);
use MongoDB ();

use Catmandu;
use Spreadsheet::Read;
use Log::Any::Adapter;
use Log::Log4perl;
use Data::Dumper;
use Hash::Merge;

our $VERSION = '0.8.1';

Log::Any::Adapter->set('Log4perl');
Log::Log4perl::init('./log4perl.conf');
my $logger = Log::Log4perl->get_logger('EBSHelper');

$logger->info("Starting main program");

# Define some paths
my $uploaddir = path( config->{appdir}, 'uploads' );
mkdir $uploaddir if not -e $uploaddir;

my $dldir = path( config->{public_dir}, 'downloads' );
mkdir $dldir if not -e $dldir;

my $fixFilesDir = path( config->{appdir}, 'fixfiles' );
die "No fix files!" unless -e $fixFilesDir;

my $cacheDir = path( config->{public_dir}, 'cache' );
die "No cache!" unless -e $cacheDir;

my $dbName = config->{app}{mongodb};
my $dbUrl  = config->{db_url};

Hash::Merge::add_behavior_spec(
    {   'SCALAR' => {
            'SCALAR' => sub { $_[0] + $_[1] },
            'ARRAY'  => sub { [ $_[0], @{ $_[1] } ] },
            'HASH'   => sub { $_[1] },
        },
        'ARRAY' => {
            'SCALAR' => sub { $_[1] },
            'ARRAY'  => sub { [ @{ $_[0] }, @{ $_[1] } ] },
            'HASH'   => sub { $_[1] },
        },
        'HASH' => {
            'SCALAR' => sub { $_[1] },
            'ARRAY'  => sub { [ values %{ $_[0] }, @{ $_[1] } ] },
            'HASH'   => sub { Hash::Merge::_merge_hashes( $_[0], $_[1] ) },
        },
    },
    'My Behavior',
);

hook before => sub {
    if ( request->path =~ m{^/api/.*} ) {
        header 'Content-Type'                => 'application/json';
        header 'Access-Control-Allow-Origin' => '*';
        header 'Access-Control-Allow-Methods' =>
          'GET, PUT, POST, OPTIONS, DELETE';
    }
};

# Serve websites
get '/' => sub {
    template 'index' => { 'title' => 'EBSHelper', _default_vars() };
};

get '/marc-data' => sub {
    template 'marc-data' =>
      { 'title' => 'EBSHelper – MARC-Daten', _default_vars() };
};

get '/acq-requests' => sub {
    template 'acq-requests' =>
      { 'title' => 'EBSHelper – Ankaufswünsche', _default_vars() };
};

get '/price-list' => sub {
    template 'price-list' =>
      { 'title' => 'EBSHelper – Preisliste', _default_vars() };
};

get '/usage' => sub {
    template 'usage' =>
      { 'title' => 'EBSHelper – Benutzungsdaten', _default_vars() };
};

get '/titles' => sub {
    template 'titles' =>
      { 'title' => 'EBSHelper – Alle Titel', _default_vars() };
};

get '/admin' => require_role Admin => sub {
    template 'admin' => { 'title' => 'EBSHelper – Admin', _default_vars() } =>
      { layout => 'adminPages' };
};

# API definitions
prefix '/api/v1' => sub {

    # Enrich & clean up MARC file, check for duplicates, save into mongo db,...
    post '/packageFiles/:ebsProject' => require_role Metadata => sub {
        header 'Content-Type' => 'application/json';

        my $ebsProject = param('ebsProject');
        if ( my $res = _return_if_proj_inactive($ebsProject) ) {
            return $res;
        }

        # load input file
        my $sigel = param('paketSigel') || '___';

        my $anysigel = $sigel ne '___' ? $sigel : $ebsProject;

        if ( !$anysigel ) {
            return _json_encode( { error => 'No sigel provided!' } );
        }

        my $timestamp = _createTimestamp();

        my $inputdir = path( $uploaddir, $anysigel );
        mkdir $inputdir unless -e $inputdir;
        my $input     = request->upload('inputFile');
        my $inputPath = path( $inputdir, $timestamp . "_" . $input->basename );
        $input->copy_to($inputPath);

        my $packageMonth = param('packageMonth');

        my $marcToday = strftime "%y%m%d", localtime;

        my %inputHash = (
            inputFile    => $inputPath,
            type         => param('inputFileType'),
            sigel        => $sigel,
            packageMonth => $packageMonth,
            today        => $marcToday,
            timestamp    => $timestamp,
            ebsProject   => $ebsProject,
            dbName       => $dbName,
        );

        my $importer = Catmandu->importer(
            'MARC',
            file => $inputPath,
            type => param('inputFileType')
        );
        my $num = $importer->count;

        $logger->info("\n$num DS in this package.\n");

        my $marcfilepath = marc2marc( \%inputHash );

        my $titleList = _mongodb( $ebsProject, 'titleList' );
        my $newNum = $titleList->count( { timestamp => $timestamp } );

        my $convMarcData = _mongodb( $ebsProject, 'packageFiles' );

        $convMarcData->insert_one(
            {
                _id      => $timestamp,
                marcFile => sprintf( "%s_%s_%s.mrc",
                    $timestamp, $anysigel, $packageMonth ),
                ebsProject   => $ebsProject,
                sigel        => $anysigel,
                packageMonth => $packageMonth,
                recordsIn    => $num,
                recordsOut   => $newNum,
                originalFile => $input->basename,
            }
        );

# TO DO: think of a smarter way to return the data
# re turn to_json { sigel => $sigel , type => $type, Monat => $packagemonth};
# if ($newNum){
#     return _json_encode({ ok => 1, sigel => $anysigel, num => $newNum });
# }
# return _json_encode({error => "Didn’t work out as expected", oldnum => $num , num => $newNum });
#return _json_encode({ ok => 1, sigel => $anysigel });
        redirect('/marc-data');
    };

    options '/packageFiles/:ebsProject/:id' => sub {
        return '';
    };

    del '/packageFiles/:ebsProject/:id' => require_role Metadata => sub {
        my $ebsProject = param('ebsProject');
        if ( my $res = _return_if_proj_inactive($ebsProject) ) {
            return $res;
        }

        # ID is timestamp
        my $id = param('id');

        my $packageFiles = _mongodb( $ebsProject, 'packageFiles' );

        $packageFiles->delete_one( { _id => $id } );

        my $titleList = _mongodb( $ebsProject, 'titleList' );
        my $numRecsToDelete = $titleList->count( { timestamp => $id } );
        $logger->info("Deleting $numRecsToDelete records");
        $titleList->delete_many( { timestamp => $id } );

        return _json_encode( { ok => 1 } );
    };

    get '/packageFiles' => sub {
        my $ebsProject = param('ebsProject');

        my $items = _mongodb( $ebsProject, 'packageFiles' );

        my @data = $items->find->all;

        return _json_encode_blessed( { items => \@data } );
    };

    get '/catFiles' => sub {
        my @filenames = query_parameters->get_all('packageFile');

        print Dumper(@filenames);

        my $outfile = _mergeFiles(@filenames);

        send_file($outfile);
    };

    get '/priceLists' => sub {
        my $ebsProject = param('ebsProject');

        my @data = _mongodb( $ebsProject, 'titleList' )->find->all;

        my @output;
        my $counter;
        foreach my $item (@data) {
            $counter++;
            push @output,
              {
                _id          => $item->{_id},
                localId      => '123456788',
                title        => $item->{title},
                authors      => $item->{author},
                isbn         => $item->{isbn},
                pisbn        => $item->{pisbn},
                doi          => $item->{doi},
                packageMonth => $item->{packageMonthe},
                sigel        => $item->{sigel},
                prices       => {
                    singleuser => 123,
                    threeusers => 369,
                    unlimusers => 999,
                },
              };

        }

        return _json_encode( { items => \@output } );
    };

    post '/priceLists/:ebsProject' => require_role Admin => sub {
        my $ebsProject = param('ebsProject');
        my $proj = _db_projects()->find_one( { 'code' => $ebsProject } );

        my $licenses = $proj->{licenses};

        my $inputdir   = path( $uploaddir, 'priceList' );
        mkdir $inputdir unless -e $inputdir;
        my $input = request->upload('inputFile');
        $logger->debug("Loading pricelist $input to $ebsProject");

        my $inputPath = path( $inputdir, "_" . $input->basename );
        $input->copy_to($inputPath);

        # parse Excel file
        my $book = Spreadsheet::Read->new( $inputPath, parser => 'xlsx' );

        my $sheet = $book->sheet(1);
        my @rows  = $sheet->rows();

        my @header       = @{ shift @rows };
        my %columns;
        $columns{doi}      = first { $header[$_] eq 'DOI' } 0 .. $#header;
        $columns{isbn}     = first { $header[$_] eq 'ISBN' } 0 .. $#header;
        $columns{pisbn}     = first { $header[$_] eq 'pISBN' } 0 .. $#header;
        $columns{currency} = first { $header[$_] eq 'Currency' } 0 .. $#header;

        foreach my $lic (@$licenses){
            my $idx = first { $header[$_] eq $lic } 0 .. $#header;
            $columns{licenses}{$lic} = $idx if $idx;
        }

        my $pkg_currency = $proj->{contractualSum}{currency};

        my %hpricelist;
        my $count = 1;

        foreach my $row (@rows) {

            my $doi = $row->[$columns{doi}];
            my $isbn = $row->[$columns{isbn}];
            my $pisbn = $row->[$columns{pisbn}];
            $doi =~ s/\x{a0}//g;
            $doi =~ s[^.*?(10\.\d{4}[\d.]*/.+?)\s+$][$1];
            next unless $doi || $isbn =~ m/^97[89]/ || $pisbn =~ m/^97[89]/;

            my $pricelistData;
            $pricelistData->{prices}{currency} = $columns{currency} ? $row->[$columns{currency}] : $pkg_currency;

            if ( $doi =~ m[(10.\d{4}[\d.]*/.+)$] ) {
                $doi = $1;
                $pricelistData->{doi} = $doi;
                $hpricelist{dois}{$doi} = $count;
            }

            if ($isbn =~ m/97[89][\d-]+/){
                $pricelistData->{isbn} = $isbn;
                $hpricelist{isbns}{$isbn} = $count;
            }

            if ($pisbn =~ m/97[89][\d]+/){
                $pricelistData->{pisbn} = $pisbn;
                $hpricelist{pisbns}{$pisbn} = $count;
            }

            foreach my $lic (@$licenses){
                $pricelistData->{prices}{licenses}{$lic} = $row->[$columns{licenses}{$lic}];
            }

            push @{$hpricelist{data}}, $pricelistData;
            $count++;
        }
            say Dumper(%hpricelist);

        my $titleList = _mongodb( $ebsProject, 'titleList' );
        my @data      = $titleList->find->all;
        my %message;

        my $null_pricelist = { currency => ''};
        for my $lic (@$licenses){
            $null_pricelist->{licenses}{$lic} = 0;
        }
        foreach my $title (@data) {
            my $doi = $title->{'doi'};
            my $isbn = $title->{isbn};
            my $pisbn = $title->{pisbn};
            my $prices;
            if ( my $pl_doi = $hpricelist{dois}{$doi} ) {
                $prices =  $hpricelist{data}[$pl_doi-1]{prices};
            } elsif ( my $pl_isbn = $hpricelist{isbns}{$isbn} ) {
                $prices = $hpricelist{data}[$pl_isbn-1]{prices};
            } elsif ( my $pl_pisbn = $hpricelist{pisbns}{$pisbn} ) {
                $prices = $hpricelist{data}[$pl_pisbn-1]{prices};
            } elsif (!$title->{prices}{licenses}) {
                $prices = $null_pricelist;
                $message{noPriceCount}++;
                push @{ $message{noPrice} }, $doi;
            }
            if ($prices){
                $titleList->update_one(
                    { _id => $title->{_id} },
                    {
                        '$set' => {
                            prices => $prices
                        }
                    }
                );
            }

        }

        return _json_encode( \%message );
    };

    get '/acqRequests' => sub {
        my $ebsProject = param('ebsProject');

        my $project = _db_projects()->find_one( { 'code' => $ebsProject } );
        my $groups = _get_groups($project);
        my $purchase_proposals = _read_purchase_proposals( $ebsProject );
        my @titles = _mongodb( $ebsProject, 'titleList' )->find()->all;

        my @project_licenses = @{ $project->{licenses} };
        my $default_license = $project->{defaultLicense};

        my ( @data, $total, $totalLicenses, $currency, %selectors );
        foreach my $title (@titles) {
            my ($purchase_proposal, $license) = _get_purchase_proposal($title, $purchase_proposals, $default_license);
            
            next if !$title->{portfolioIntDesc} && !%$purchase_proposal;

            $currency = $title->{prices}{currency} unless $currency;

            my %prices = %{$title->{prices}{licenses}};

            for my $pl (@project_licenses) {
                $total->{$pl} += $prices{$pl};
            }

            my $price = $prices{$license};

            if ( $purchase_proposal->{purchaseProposal} ) {
                $selectors{ $purchase_proposal->{purchaseProposal} }{price} += $price;
                $selectors{ $purchase_proposal->{purchaseProposal} }{books}++;
            }
            $totalLicenses += $price;

            my $data = _build_record($title, $license, $purchase_proposal, $groups);
            $data->{price} = $price;
            push @data, $data;
        }

        my $conversion_table = { GBP_EUR => 1.1668, EUR_GBP => 0.8571 };
        $total->{currency}      = $currency;
        $total->{totalLicenses} = $totalLicenses;

        my @columns = _build_columns(
            qw(
            ISBN isbn
            Autoren authors
            Titel title
            Jahr year
            Ankaufswunsch purchaseProposal
            )
        );

        if ( $#project_licenses > 1 ) {
            push @columns, _build_columns("Lizenz", "license");
            foreach my $pl ( @project_licenses ) {
                push @columns, _build_columns( "Preis $pl", "prices.$pl" );
            }
        }

        push @columns, _build_columns(
            qw(
                Preis price
                Paket sigel
            )
        );

        if ($groups) {
            push @columns, _build_columns(qw/ Gruppe group/);
        }

        return _json_encode(
            {
                items           => \@data,
                total           => $total,
                conversionTable => $conversion_table,
                selectors       => \%selectors,
                columns         => \@columns,
                licenses        => \@project_licenses,
                _default_vars()
            }
        );
    };

    del '/purchaseProposal' => require_role 'Selector' => sub {
        my $ebsProject = body_parameters->get('ebsProject');

        if ( my $res = _return_if_proj_inactive($ebsProject) ) {
            return $res;
        }

        my $id = body_parameters->get('id');

        my $user_attribute =
            logged_in_user->{team}
          ? logged_in_user->{team}
          : logged_in_user->{initials};
        my $purchase_proposals = _mongodb( $ebsProject, 'purchaseProposals' );

        my $rec;
        if ( $rec = $purchase_proposals->find_one( { _id => $id } ) ) {

            if ( $rec->{user} eq $user_attribute 
                || user_has_role('SelectionManager') 
                || user_has_role('Admin') ) {
                if ( my $res =
                    $purchase_proposals->delete_one( { _id => $id } ) )
                {
                    return _json_encode( { ok => 1 } );
                } else {
                    return _json_encode(
                        { error => "Couldn’t delete proposal in DB!" } );
                }
            } else {
                return _json_encode(
                    { error => "Can’t delete foreign proposal" } );
            }
        } else {
            return _json_encode( { error => "Couldn’t find record in DB." } );
        }
    };

    post '/purchaseProposal' => require_role 'Selector' => sub {
        my $ebsProject = body_parameters->get('ebsProject');

        if ( my $res = _return_if_proj_inactive($ebsProject) ) {
            return $res;
        }

        my $id   = body_parameters->get('id');
        my $isbn = body_parameters->get('isbn');

        my $user_attribute =
            logged_in_user->{team}
          ? logged_in_user->{team}
          : logged_in_user->{initials};
        my $purchase_proposals = _mongodb( $ebsProject, 'purchaseProposals' );
        my $titleList          = _mongodb( $ebsProject, 'titleList' );

        if ($isbn) {
            $isbn =~ s/[^\dX]//ig;
            if ( my $rec = $titleList->find_one( { 'isbn' => qr/$isbn/ } ) ) {
                $id = $rec->{_id};
            } elsif ( $rec = $titleList->find_one( { 'pisbn' => qr/$isbn/ } ) )
            {
                $id = $rec->{_id};
            } else {
                return _json_encode(
                    { warning => "Couldn’t locate ISBN in packages" } );
            }
        }

        if ( !$id ) {
            return _json_encode( { error => 'No id found!' } );
        } elsif ( $purchase_proposals->find_one( { _id => $id } ) ) {
            return _json_encode(
                { warning => 'Purchase proposal already exists' } );
        } elsif (
            my $res = $purchase_proposals->insert_one(
                { _id => $id, user => $user_attribute }
            )
          )
        {
            return _json_encode( { ok => 1, name => $user_attribute } );

        } else {
            return _json_encode(
                { error => 'Couldn‘t create proposal in DB!' } );
        }
    };

    post '/concurrentUsers' => require_role 'Selector' => sub {
        my $ebsProject = body_parameters->get('ebsProject');

        if ( my $res = _return_if_proj_inactive($ebsProject) ) {
            return $res;
        }

        my $id      = body_parameters->get('id');
        my $license = body_parameters->get('license');

        #my $user               = logged_in_user;
        my $user_attribute =
            logged_in_user->{team}
          ? logged_in_user->{team}
          : logged_in_user->{initials};
        my $purchase_proposals = _mongodb( $ebsProject, 'purchaseProposals' );

        if ( !$id ) {
            return _json_encode( { error => 'No id transmitted!' } );
        } elsif ( my $rec = $purchase_proposals->find_one( { _id => $id } ) ) {

            if ( $rec->{user} ne $user_attribute ) {
                return _json_encode(
                    { error => "Can’t modify foreign proposal" } );
            } elsif (
                my $res = $purchase_proposals->update_one(
                    { '_id'  => $id },
                    { '$set' => { 'license' => $license } }
                )
              )
            {
                return _json_encode( { ok => 1 } );
            }
            return _json_encode( { error => 'Something went wrong.' } );
        }
        return _json_encode( { error => 'Couldn‘t find proposal in DB!' } );
    };

    get '/usage' => sub {
        my $ebsProject = param('ebsProject');

        my $project    = _db_projects()->find_one( { 'code' => $ebsProject } );
        my $groups = _get_groups($project);
        my $purchase_proposals = _read_purchase_proposals($ebsProject);
        my @titles = _mongodb( $ebsProject, 'titleList' )
          ->find( { "usage" => { '$exists' => 1 } } )->all;
        my $months = _get_months($project);
        my $default_license = $project->{defaultLicense};

        my @data;
        foreach my $title (@titles) {
            my ($purchase_proposal, $license) = _get_purchase_proposal($title, $purchase_proposals, $default_license);

            my %usage         = %{ $title->{usage} };

            my %output_usage;
            my ( $month_counter, $usage_total, $used_months_counter );
            foreach my $month (@$months) {
                if ( $month lt $title->{packageMonth} ) {
                    $output_usage{$month} = 'NV';
                } elsif ( $usage{$month} ) {
                    $output_usage{$month} = $usage{$month} + 0;
                    $month_counter++;
                    $usage_total += $usage{$month};
                    $used_months_counter++ if $usage{$month} > 0;
                } else {
                    $output_usage{$month} = 0;
                    $month_counter++;
                }
            }

            my $usage_href = {
                usageTotal       => $usage_total,
                usedMonths       => $used_months_counter,
                usageRelative =>
                  sprintf( "%.3f", $usage_total / $month_counter ),
                usedMonthsRelative =>
                  sprintf( "%.3f", $usage_total / $used_months_counter ),
                usageData        => \%output_usage,
                activeMonths     => $month_counter,
            };
            my $record = _build_record($title, $license, $purchase_proposal, $groups);

            $record->{$_} = $usage_href->{$_} for keys %$usage_href;

            push @data, $record;
        }

        $logger->info( scalar @data, ' Datensätze in @data' );

        push my @columns, _build_columns(
            qw/
              Ankaufswunsch purchaseProposal
              ISBN isbn
              Autoren authors
              Titel title
              Jahr year
              Paket sigel/
        );

        if ($groups) {
            push @columns, _build_columns(qw/ Gruppe group/);
        }

        push @columns, _build_titled_columns(
                'N. ges.', 'usageTotal', 'Nutzung gesamt',
                'N. Ø', 'usageRelative', 'Durchschnittliche monatliche Nutzung',
                'Akt. Mon.', 'activeMonths', 'Zahl der Monate, während der der Titel aktiv war',
                'Gen. Mon.', 'usedMonths', 'Zahl der Monate, in denen der Titel genutzt wurde',
                'N. Ø/gen. Mon.', 'usedMonthsRelative', 'Durchschnittliche Nutzung des Titels während der genutzten Monate'
        );
        foreach my $pl ( @{ $project->{licenses} } ) {
            push @columns, _build_columns( "Preis $pl", "prices.$pl" );
        }

        foreach my $month (@$months){
            push @columns, _build_columns($month, "usageData.$month");
        }

        push @columns, _build_columns( "P-ISBN", "pisbn" );

        return _json_encode( { items => \@data, columns => \@columns } );
    };

    get '/titles' => sub {
        my $ebsProject = param('ebsProject');

        my $project    = _db_projects()->find_one( { 'code' => $ebsProject } );
        my $groups = _get_groups($project);
        my $purchase_proposals = _read_purchase_proposals($ebsProject);
        my @titles = _mongodb( $ebsProject, 'titleList' )->find()->all;
        my $default_license = $project->{defaultLicense};

        my @data;
        foreach my $title (@titles) {
            my ($purchase_proposal, $license) = _get_purchase_proposal($title, $purchase_proposals, $default_license);

            my $record = _build_record($title, $license, $purchase_proposal, $groups);
            $record->{usageFlag} = $title->{usage} ? 'U' : '';

            push @data, $record;
        }

        $logger->info( scalar @data, ' Datensätze in @data' );

        push my @columns, _build_columns(
            qw/
              U usageFlag
              Ankaufswunsch purchaseProposal
              ISBN isbn
              Autoren authors
              Titel title
              Jahr year
              Paket sigel/
        );

        if ($groups) {
            push @columns, _build_columns(qw/ Gruppe group/);
        }

        foreach my $pl ( @{ $project->{licenses} } ) {
            push @columns, _build_columns( "Preis $pl", "prices.$pl" );
        }

        push @columns, _build_columns( "P-ISBN", "pisbn" );

        return _json_encode( { items => \@data, columns => \@columns } );
    };

    get '/packages/:ebsProject' => sub {
        my $ebsProject = param('ebsProject');

        my $project = _db_projects()->find_one( { 'code' => $ebsProject } );

        my $output;
        foreach my $package ( @{ $project->{packages} } ) {
            $output->{ $package->{sigel} } = { name => $package->{name}, };
        }

        return _json_encode( { packages => $output } );
    };

    get '/ebsProjects' => sub {
        return _json_encode_blessed(
            { items => [ _db_projects()->find()->all ] } );
    };

    get '/ebsProjects/:ebsProject/statistics' => sub {
        my $ebsProject = param('ebsProject');

        my $project = _db_projects()->find_one( { 'code' => $ebsProject } );
        my $groups = _get_groups($project);
        my $purchase_proposals = _read_purchase_proposals($ebsProject);
        my @titles = _mongodb( $ebsProject, 'titleList' )->find()->all;
        my $months = _get_months($project);
        my $default_license = $project->{defaultLicense};

        my ( %data, $grp_data, @data, %columns, $has_print, $has_inst );

        foreach my $title (@titles) {
            my $package = $title->{sigel};

            $data{$package}{size}++;

            if ( $title->{usage} ) {
                $data{$package}{usageTitles}{total}++;
                my %usage = %{ $title->{usage} };
                foreach my $month (@$months) {
                    if ( my $u_m = $usage{$month} ) {
                        $data{$package}{usage}{$month} += $u_m;
                        $data{$package}{usage}{total}  += $u_m;
                        $data{$package}{usageTitles}{months}{$month}++;
                    }
                }
            }

            my ( $purchase_proposal, $license ) = _get_purchase_proposal( $title, $purchase_proposals, $default_license );

            my $price = $title->{prices}{licenses}{$license};

            if ($purchase_proposal){
                if ($purchase_proposal->{purchaseProposal}){
                    $data{$package}{acqRequests}{selectors}++;
                    $data{$package}{acqRequests}{selectorsPrice} += $price;
                } elsif ($purchase_proposal->{institut}){
                    $data{$package}{acqRequests}{institute}++;
                    $data{$package}{acqRequests}{institutePrice} += $price;
                    $has_inst++;
                }
                if ($purchase_proposal->{print}){
                    $data{$package}{acqRequests}{print}++;
                    $has_print++;
                }
            }
        }

        foreach my $package ( keys %data ) {
            $data{$package}{usageTitles}{total}
                = 0 unless $data{$package}{usageTitles}{total};

            if ($groups){
                my $group = $groups->{$package};
                $grp_data->{$group} = Hash::Merge::merge(\%{$grp_data->{$group}}, $data{$package});
                $grp_data->{$group}{group} = $group;
                $data{$package}{group} = $group;
            }
            $data{$package}{pkgName} = $package;
            push @data, $data{$package};
        }
        push my @group_data, values %$grp_data;

        foreach my $month (@$months){
            push @{$columns{usage}}, _build_columns($month, "usage.$month");
            push @{$columns{usageTitles}}, _build_columns($month, "usageTitles.months.$month");
        }

        push @{$columns{usage}}, _build_columns('Gesamt', "usage.total");
        push @{$columns{usageTitles}}, _build_columns('Gesamt', "usageTitles.total");

        push @{$columns{titles}}, _build_columns('Titel', 'size');

        if ($has_print){
            push @{$columns{acqRequests}}, _build_columns(
                'Print', 'acqRequests.print'
            );
        }
        if ($has_inst){
            push @{$columns{acqRequests}}, _build_columns(
                'Institute', 'acqRequests.institute',
                'Preis (I)', 'acqRequests.institutePrice',
            );
        }
        push @{$columns{acqRequests}}, _build_columns(
            'ReferentInnen', 'acqRequests.selectors',
            'Preis (Ref)', 'acqRequests.selectorsPrice'
        );

        push @{$columns{packages}}, _build_columns('Paket', 'pkgName');
        push @{$columns{packages}}, _build_columns('Gruppe', 'group') if $groups;

        say Dumper($grp_data);
        return _json_encode( { items => \@data, grpItems => \@group_data, columns => \%columns } );
    };

    get '/ebsProjects/:ebsProject' => sub {
        my $ebsProject = param('ebsProject');
        return _json_encode_blessed(
            _db_projects()->find_one( { 'code' => $ebsProject } ) );
    };

    del '/ebsProjects/:ebsProject' => require_role 'Admin' => sub {
        my $ebsProject = param('ebsProject');
        return _json_encode(
            {
                deleted_records => _db_projects()
                  ->delete_one( { 'code' => param('ebsProject') } )
                  ->{deleted_count}
            }
        );
    };

    put '/ebsProjects/:ebsProject/toggleStatus' => require_role Admin => sub {
        my $ebsProject = param('ebsProject');
        my $status     = body_parameters->get('status') eq 'true' ? 1 : 0;

        my $result = _db_projects()->update_one(
            { 'code' => $ebsProject },
            {
                '$set' => {
                    'active'       => $status,
                    'timeModified' => time()
                }
            }
        );

        if ( $result->acknowledged ) {
            return _json_encode( { ok => 1, value => $status } );
        }
        return _json_encode(
            { warning => "Couldn’t find project $ebsProject" } );
    };

    post '/ebsProjects' => require_role Admin => sub {

        # debug 'All params: '          . Dumper { params };
        my $ebsProject = body_parameters->get('ebsProject');

        my $runtime_start = body_parameters->get('runtimeStart');
        my $runtime_end   = body_parameters->get('runtimeEnd');

        my $input = {
            name         => body_parameters->get('projectName'),
            code         => body_parameters->get('projectCode'),
            runtimeStart => $runtime_start,
            runtimeEnd   => $runtime_end,
            runtimeMonths =>
              _createRuntimeMonths( $runtime_start, $runtime_end ),
            contractualSum => {
                sum      => body_parameters->get('contractualSum'),
                currency => body_parameters->get('currency')
            },
            active => body_parameters->get('ebsProjectActive') ? true : false,
            reportPlatform => body_parameters->get('reportPlatform'),
            timeCreated    => time(),
            groups => body_parameters->get('hasGroups') ? true : false,
        };
        if ( body_parameters->get('basePackage') ) {
            $input->{basePackage} = {
                month    => body_parameters->get('basePackage'),
                operator => body_parameters->get('basePackageOperator'),
            };
        }

        my @packages = split /\s*\r*\n/, body_parameters->get_all('packages');
        foreach my $pkg (@packages) {
            next unless $pkg =~ m/\w+/;
            my ( $name, $sigel, $group, $short, $abbr ) = split /\s*::\s*/,
              $pkg;
            $sigel =~ s/\s*(\S)\s*/$1/;
            my $package_href = {
                name  => $name,
                sigel => $sigel,
            };
            $package_href->{short} = $short ? $short : $name;
            $package_href->{abbr}  = $abbr  ? $abbr  : $name;
            $package_href->{group} = $group if $group;
            push @{ $input->{packages} }, $package_href;
        }

        my @licenses = split /\s*\r*\n/, body_parameters->get_all('licenses');
        foreach my $license (@licenses){
            chomp $license;
            push @{ $input->{licenses} }, $license;
        }

        # read default license from input or take first of licenses array
        my $default_license = body_parameters->get('defaultLicense') 
            ? body_parameters->get('defaultLicense')
            : ${ $input->{licenses} }[0];

        $input->{defaultLicense} = $default_license;

        if ( my $res = _db_projects()->insert_one($input) ) {
            return _json_encode( { ok => 1 } );
        }

        return _json_encode( { error => 'Couldn‘t create project in DB!' } );
    };

};

# Auxiliary methods

sub marc2marc {
    my $inputHref = shift;

    my $sigel      = $inputHref->{sigel};
    my $ebsProject = $inputHref->{ebsProject};

    my $filename = sprintf( "%s_%s_%s.mrc",
        $inputHref->{timestamp},
        $sigel ne '___' ? $sigel : $ebsProject,
        $inputHref->{packageMonth} );

    my $marcOutput = path( $dldir, $filename );

    my $inputFile = $inputHref->{inputFile};

    # Convert from MARC 8 if necessary
    open( my $inputMARC, '<', $inputFile )
      or $logger->error("Could not open file $inputFile");
    my $encodingMark = substr <$inputMARC>, 9, 1;
    if ( $encodingMark =~ m/ / ) {
        $logger->info("MARC 8 encoding found: $encodingMark Converting input.");
qx{yaz-marcdump -f MARC-8 -t UTF-8 -o marc -l 9=97 $inputFile >uploads/tmp.mrc};
        rename 'uploads/tmp.mrc', $inputFile;
    }

    my $variables = {
        today        => $inputHref->{today},
        timestamp    => $inputHref->{timestamp},
        ebsProject   => $ebsProject,
        sigel        => $sigel,
        packageMonth => $inputHref->{packageMonth},
        idCache      => $inputHref->{idCache},
        dbName       => $inputHref->{dbName},
        dbUrl        => $dbUrl,
        bagTitleList => _mongoCollName( $inputHref->{ebsProject}, 'titleList' ),
        MARC2ISO     => path( $fixFilesDir, 'marc2iso3166H.csv' ),
        ISO2MARC     => path( $fixFilesDir, 'iso3166H2marc.csv' ),
    };

    print Dumper( "input received" => $variables );

    my $marcImporter = Catmandu->importer(
        'MARC',
        file => $inputFile,
        type => $inputHref->{type},
    );

    my $marcFixer = Catmandu::Fix->new(
        variables => $variables,
        fixes     => [
            path( $fixFilesDir, 'checkDuplicates.fix' ),
            path( $fixFilesDir, 'ebook.fix' ),
            path( $fixFilesDir, 'exportToMongo.fix' )
        ],
    );

    my $marcExporter = Catmandu->exporter( 'MARC', file => $marcOutput );

    $marcExporter->add_many( $marcFixer->fix($marcImporter) );    #->benchmark);
    $marcExporter->commit;

    undef($marcExporter);

    return $marcOutput;
}

sub _mongoCollName {
    my $collName = join ".", @_;
    $collName =~ s/-/_/g;
    return $collName;
}

sub _mongodb {
    my $collection = _mongoCollName(@_);

    my $client = MongoDB::MongoClient->new( host => $dbUrl, port => 27017 );
    my $db     = $client->get_database($dbName);
    return $db->get_collection($collection);
}

sub _mergeFiles {

    my @files     = @_;
    my $timestamp = _createTimestamp();
    my $outfile   = path( $dldir, $timestamp . '.mrc' );

    open my $output, '>>', $outfile
      or die "Could not open '$outfile' for appending\n";

    $logger->debug("Merging files in $outfile");

    foreach my $file (@files) {
        next unless $file =~ m/\.mrc$/;
        $file = path( $dldir, $file );
        if ( open my $in, '<', $file ) {
            while ( my $line = <$in> ) {
                print $output $line;
            }
            close $in;
        } else {
            warn "Could not open '$file' for reading\n";
        }
    }
    close $output;
    my $relname = "downloads/$timestamp.mrc";
    print $relname . "\n\n";
    return $relname;
}

sub _createTimestamp {
    return strftime "%y%m%d-%H%M%S", localtime;
}

sub _createRuntimeMonths {
    my ( $month, $end_month ) = @_;

    my $months;
    while ( $month le $end_month ) {
        $months->{$month}->{usage} = 0;
        my ( $yyyy, $mm ) = split '-', $month;
        if ( $mm < 12 ) {
            $mm++;
        } else {
            $yyyy++;
            $mm = 1;
        }
        $month = sprintf '%4d-%02d', $yyyy, $mm;
    }
    return $months;
}

sub _default_vars {
    return (
        (
            app_version => $VERSION,
            defined(logged_in_user)
            ? (
                user_logged_in => defined(logged_in_user),
                user_name      => logged_in_user->{name},
                user_initials  => logged_in_user->{initials},
                user_attribute => logged_in_user->{team}
                ? logged_in_user->{team}
                : logged_in_user->{initials}
              )
            : ()
        )
    );
}

sub _db_projects {
    return _mongodb( 'ebsHelper', 'projects' );
}

sub _new_json {
    return JSON::MaybeXS->new;
}

sub _json_encode {
    return _new_json()->utf8(1)->encode(@_);
}

sub _json_encode_blessed {
    return _new_json()->utf8(1)->convert_blessed(1)->encode(@_);
}

sub _return_if_proj_inactive {
    my $ebs_project = shift;
    if ( !_db_projects()->find_one( { 'code' => $ebs_project } )->{active} ) {
        return _json_encode( { error => 'Can’t update inactive project' } );
    }

    return;
}

sub _get_groups {
    my $project = shift;
    if ($project->{groups}) {
        return { map { $_->{sigel} => $_->{group} } @{ $project->{packages} } };
    }
    return;
}

sub _read_purchase_proposals {
    my $ebsProject = shift;
    return  {map {$_->{_id} => $_} _mongodb( $ebsProject, 'purchaseProposals' )->find()->all};
}

sub _get_months {
    my $project = shift;
    my %runtime_months = %{ $project->{runtimeMonths} };
    my @keys           = sort { $a cmp $b } keys %runtime_months;
    my ( $months, $month_count );
    foreach my $month (@keys) {
        $month_count++;
        push @$months, $month if $runtime_months{$month}{usage};
    }
    return $months;
}

sub _get_purchase_proposal {
    my ($title, $purchase_proposals, $default_license) = @_;
    my %purchase_proposal;
    my $license = '';
    my $pp = $purchase_proposals->{$title->{_id}};

    if ( $title->{portfolioIntDesc} ) {
        if ( $title->{portfolioIntDesc} =~ m/PRINT/i ) {
            $purchase_proposal{print} = $title->{portfolioIntDesc};
        } else {
            $purchase_proposal{institut} = $title->{portfolioIntDesc};
        }
    }
    if ($pp) {
        $purchase_proposal{purchaseProposal} = $pp->{user};
    }

    if ($purchase_proposal{purchaseProposal} || $purchase_proposal{insitut}) {
        $license = $pp->{license} || $default_license;
    }
    return (\%purchase_proposal, $license);
}

sub _build_columns {
    my @cols;

    while ( my ( $i, $j ) = splice( @_, 0, 2 ) ) {
        push @cols, { name => $i, data => $j, tooltip => $i };
    }
    return @cols;
}

sub _build_titled_columns {
    my @cols;

    while ( my ( $i, $j, $k ) = splice( @_, 0, 3 ) ) {
        push @cols, { name => $i, data => $j, tooltip => $k };
    }
    return @cols;
}

sub _build_record {
    my ($title, $license, $purchase_proposal, $groups) = @_;
    my $isbn = $title->{isbn};
    $isbn =~ s/.*?(\d{13}).*/$1/;

    my $prices = $title->{prices}{licenses};
    delete $prices->{currency};

    my $item_title = $title->{title};
    $item_title =~ s/<<(.+)>>/$1/;
    my $record = {
        id               => $title->{_id},
        portfolioIntDesc => $title->{portfolioIntDesc},
        localId          => $title->{localId},
        title            => $item_title,
        authors          => $title->{author},
        isbn             => $isbn,
        pisbn            => $title->{pisbn},
        doi              => $title->{doi},
        packageMonth     => $title->{packageMonth},
        sigel            => $title->{sigel},
        currency         => $title->{prices}{currency},
        prices           => $prices,
        year             => $title->{year},
        purchaseProposal => $purchase_proposal,
        license          => $license,
    };
    if ($groups) {
        $record->{group} = $groups->{ $title->{sigel} };
    }
    return $record;
}

true;

