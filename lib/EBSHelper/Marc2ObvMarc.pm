package EBSHelper::pubMarc2ObvMarc;
use Dancer2;
use Catmandu;
our $VERSION = '0.1';

my $fixFilesDir = path( config->{appdir}, 'fixfiles' );

sub marc2marc {
	my $sigel     = shift;
	my $type      = shift;
	my $inputpath = shift;
	my $dldir     = shift;

	# TODO: my $today = ...;

	my $marcOutput = "$dldir/output.mrc";

	# my $marcOutput = 'output.mrc';

	my $marcImporter =
	  Catmandu->importer( 'MARC', file => $inputpath );    #, type => $type);
	   # my $marcImporter  = Catmandu->importer('MARC', file => path(config->{appdir}, 'uploads/test.mrc'));

	my $marcFixer = Catmandu::Fix->new(
		variables => {
			sigel => $sigel,

			# variables => { sigel => 'ZDB-28-OSL'},
			MARC2ISO => path( $fixFilesDir, 'marc2iso3166H.csv' ),
			ISO2MARC => path( $fixFilesDir, 'iso3166H2marc.csv' ),
			today    => '20181122',
		},
		fixes => [ path( $fixFilesDir, 'ebook.fix' ) ],

		#fixes => ['marc_add(912,a, {{sigel}})'],
	);

	my $marcExporter = Catmandu->exporter( 'MARC', file => $marcOutput );

	# my $fixed_marcImporter = $marcFixer->fix($marcImporter);

	#$marcExporter->add_many($fixed_marcImporter);#->benchmark);
	$marcExporter->add_many( $marcFixer->fix($marcImporter) );    #->benchmark);
	$marcExporter->commit;

	undef($marcExporter);

	return $marcOutput;
}

1;
