package EBSHelper::Marc2json;

use Dancer2;
use Catmandu;
use Data::Dumper;

our $VERSION = '0.1';
my $fixFilesDir = path( config->{appdir},     'fixfiles' );
my $dldir       = path( config->{public_dir}, 'downloads' );

sub marc2json {
	my $sigel      = shift;
	my $type       = shift;
	my $inputpath  = shift;
	#my $dldir      = shift;
	my $jsonOutput = "$dldir/output1.json";

	my $jsonImporter =
	  Catmandu->importer( 'MARC', file => $inputpath );    #, type => $type);

	my $jsonFixer = Catmandu::Fix->new(
		variables => { sigel => $sigel, },

		# fixes => ['add_field(foo, {{sigel}})']
		fixes => [ path( $fixFilesDir, 'IDs2json.fix' ) ],
	);

	my $jsonExporter       = Catmandu->exporter( 'JSON', file => $jsonOutput );
	my $fixed_jsonImporter = $jsonFixer->fix($jsonImporter);

	$jsonExporter->add_many( $fixed_jsonImporter->benchmark );

	$jsonExporter->commit;
	undef($jsonExporter);

	# TODO: save json in mongo
	# TODO: create cache file?
	#

	return $jsonOutput;
}

marc2json(@_);

1;
