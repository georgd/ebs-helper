# EBS-Helper

A web application, assembling utilities for use with evidence based acquisition models.

## Features

- MARC data conversion using Catmandu
- Usage data harvesting from Alma Analytics
- Price list upload
- Presentation of usage data together with prices and acquisition request details
- Mark items as proposed for acquisition

## Installation

The EBS Helper can be run as a Docker image. It requires the Dancer2 Docker image which in turn needs the catmandu-img Docker image. Furthermore, it is built to use a MongoDB database for which there’s an official docker image.

