FROM dancer2
MAINTAINER Georg Mayr-Duffner "georg.mayr-duffner@wu.ac.at"

RUN apt-get update && apt-get install -y --no-install-recommends \
    libyaz5

COPY . /opt/ebs-helper
WORKDIR /opt/ebs-helper

RUN sed -i 's|http://localhost:5000|/ebs-helper|g' public/javascripts/EBSHelper.js views/*.tt views/layouts/main.tt  public/javascripts/EBSHelper_admin.js views/layouts/adminPages.tt

EXPOSE 5000

ENTRYPOINT [ "plackup", "-r", "bin/app.psgi", "-E", "production", "--port 5000", "--host 0.0.0.0" ]
