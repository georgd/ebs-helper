package EBSHelper;
use Dancer2;
# use EBSHelper::pubMarc2json;
use Catmandu;

our $VERSION = '0.1';

hook before => sub {
    if (request->path =~ m{^/cleanup}) {
        header 'Content-Type' => 'application/json';
    }
    if (request->path =~ m{^/cleanup}) {
        header 'Access-Control-Allow-Origin' => '*';
        header 'Access-Control-Allow-Methods' => 'GET, POST, OPTIONS, DELETE';
    }
};

get '/' => sub {
    template 'index' => { 'title' => 'EBSHelper' };
};

get '/api/v1/greeting' => sub {
    header 'Content-Type' => 'application/json';
    return to_json { text => 'Hello World' };
};

get '/v1' => sub {
    return template 'v1';
};

get '/upload' => sub {
    return template 'upload';
};

post '/upload' => sub {
    my $text = param('text') or die "no Text provided!";
    my $data = request->upload('file');
    my $dir = path(config->{appdir}, 'uploads');
    mkdir $dir if not -e $dir;
 
    my $path = path($dir, $data->basename);
    if (-e $path) {
        return "'$path' already exists";
    }
    $data->link_to($path);
    return $text;
};

# get '/cleanupMarc' => sub{
#     # template 'index' => { 'title' => 'EBSHelper' };
#     #return to_json { text => 'Hello World' };
#     return template 'cleanupMarc';
# };
post '/cleanupMarc' => sub {
#header 'Content-Type' => 'application/json';
    my $sigel = param('paketSigel');
    my $type  = param('inputFileType');
    my $packagemonth = param('packageMonth');

    my $input = request->upload('inputFile');
    my $uploaddir   = path(config->{appdir}, 'uploads');
    mkdir $uploaddir if not -e $uploaddir;
    my $inputpath  = path($uploaddir, $input->basename);
    $input->link_to($inputpath);
    # $input->copy_to($uploaddir);

    my $dldir = path(config->{appdir}, 'public/downloads');
    mkdir $dldir if not -e $dldir;
    # my %inputhash = (
    #     sigel => $sigel,
    #     type  => $type,
    #     packagemonth => $packagemonth,
    #     inputpath => $inputpath,
    #     downloaddir => $dldir,
    # );

    #marc2json \%inputhash;
    # EBSHelper::pubMarc2json::marc2json(%inputhash);
    # EBSHelper::pubMarc2ObvMarc::convert(%inputhash);

    my $output = "$dldir/output.mrc";

    my $jsonfilepath = marc2json($sigel, $type, $inputpath, $dldir, $packagemonth);
    my $marcfilepath = marc2marc($sigel, $type, $inputpath, $dldir, $packagemonth);
    
    redirect('/');
    # return to_json { ok => 1, msg => $output1 };
};

get 'addAcqRequest' => sub {
    header 'Content-Type' => 'application/json';
    my $isbn = param('isbn');
    my $title = param('title');
    return to_json { isbn => $isbn, title => $title };
}; 
post 'addAcqRequest' => sub {
    my $isbn = param('isbn');
    my $title = param('title');
    return {  isbn => $isbn, title => $title };
}; 

sub marc2json {
    my $sigel = shift;
    my $type = shift;
    my $inputpath = shift;
    my $dldir = shift;
    
    my $jsonOutput = "$dldir/output1.json";
 
    my $jsonImporter = Catmandu->importer('MARC', file => $inputpath, type => $type);
    
    my $jsonFixer = Catmandu::Fix->new(
        variables => { sigel => $sigel, },
        fixes => ['fixfiles/IDs2json.fix'],
    );
    my $jsonExporter = Catmandu->exporter('JSON', file => $jsonOutput);
    my $fixed_jsonImporter = $jsonFixer->fix($jsonImporter);
    $jsonExporter->add_many($fixed_jsonImporter->benchmark);
    $jsonExporter->commit;
    undef($jsonExporter);
    return $jsonOutput;
}

sub marc2marc {
    my $sigel = shift;
    my $type = shift;
    my $inputpath = shift;
    my $dldir = shift;

    my $marcOutput = "$dldir/output.mrc";
    my $marcImporter  = Catmandu->importer('MARC', file => $inputpath, type => $type);
    my $marcFixer = Catmandu::Fix->new(
        variables => { sigel => $sigel,
                       MARC2ISO => 'fixfiles/marc2iso3166H.csv',
                       ISO2MARC => 'fixfiles/iso3166H2marc.csv',
                       today => '20181122',},
        fixes => ['fixfiles/ebook.fix'],
    );
    my $marcExporter = Catmandu->exporter('MARC', file => $marcOutput);
    my $fixed_marcImporter = $marcFixer->fix($marcImporter);
    $marcExporter->add_many($fixed_marcImporter->benchmark);
    $marcExporter->commit;
    undef($marcExporter);
    return $marcOutput;
}
true;
