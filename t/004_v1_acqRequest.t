use strict;
use warnings;

use EBSHelper;
use Test::More tests =>  1;
use Plack::Test;
use HTTP::Request::Common;
# use Test::NoWarnings;
use JSON::MaybeXS qw(decode_json);
use MongoDB ();
use Data::Dumper;

subtest v1_acqRequest => sub {
	plan tests => 6;

	my $app = EBSHelper->to_app;

	my $test = Plack::Test->create($app);

	my $db_name = 'ebs-helper' . $$ . '-' . time;
	diag $db_name;
	EBSHelper->config->{app}{mongodb} = $db_name;

	my $res = $test->request(
		POST '/api/v1/acqRequests',
		{
			isbn       => 9781234567890,
			title      => 'Test-Titel',
			refCode    => 'G123/XYZ',
			notes      => 'Dringende Bestellung!',
			isInRepo   => '',
			addedOn    => '2018-12-07',
			ebsProject => 'oso_Test',
		}
	);
	ok $res->is_success, '[POST /] successful';
	is_deeply decode_json( $res->content ),
	  {
		ok       => 1,
		isbn     => 9781234567890,
		title    => 'Test-Titel',
		refCode  => 'G123/XYZ',
		notes    => 'Dringende Bestellung!',
		addedOn  => '2018-12-07',
		isInRepo => '',
	  };

	is $res->header('Content-Type'), 'application/json';
	is $res->header('Access-Control-Allow-Origin'), '*';

	my $get1 =
	  $test->request( GET '/api/v1/acqRequests?ebsProject=oso_Test'  );
	my $items1 = decode_json( $get1->content );
	print Dumper($items1);
	is scalar @{ $items1->{items} }, 1;
	is $items1->{items}[0]{title}, 'Test-Titel';
	# is scalar @{ $items1->{title} }, 1;
	# is $items1->{items}[0]{title}, 'Test-Titel';

	my $client =
	  MongoDB::MongoClient->new( host => 'localhost', port => 27017 );
	my $db = $client->get_database($db_name);
	$db->drop;
};
