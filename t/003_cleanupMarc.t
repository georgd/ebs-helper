use strict;
use warnings;

use EBSHelper;
use Test::More tests => 1;
use Plack::Test;
use HTTP::Request::Common;
use JSON::MaybeXS;

subtest cleanupMarc => sub {
	plan tests => 1;

	my $app = EBSHelper->to_app;

	my $test = Plack::Test->create($app);
	my $res  = $test->request(
		POST '/cleanupMarc',
		Content_Type => 'form-data',
		Content      => [
			paketSigel    => 'TEST-OSO',
			inputFileType => 'RAW',
			packageMonth  => '2018-10',
			inputFile     => ['t/oso_law_102018_v2.mrc'],
		]
	);

	# ok $res->is_success, '[POST /cleanupMarc] successful';

	#  is_deeply decode_json($res->content), { sigel => 'TEST-OSO' , type => 'RAW' , Monat => '2018-10'};
	# is $res->header('Content-Type'), 'application/json';
	is $res->header('Content-Type'), 'text/html; charset=utf-8';
};

