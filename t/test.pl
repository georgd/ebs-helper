use Catmandu;

main();
sub main {
    my $sigel = 'TestSigel';

    my $input = 'test.mrc';

    my ($json, $num) = marc2json($sigel, $input);
    print "$num records in $input\n";
    print "json-file: $json\n";
    print "after jsonifying, input is $input\n";
    
    my $marcfile = marc2marc($sigel, $input);
    print "marc-file: $marcfile\n"
};

sub marc2json {
    my $sigel = shift;
    my $input = shift;
    
    my $jsonOut = 'output.json';
    print "converting $input to $jsonOut (MARC to JSON)\n";
 
    my $jsonImporter = Catmandu->importer('MARC', file => $input);
    
    my $num = $jsonImporter->count;
    my $jsonFixer = Catmandu::Fix->new(fixes => ['add_field(foo,bar)']);
    my $jsonExporter = Catmandu->exporter('JSON', file => $jsonOut);
    $jsonExporter->add_many($jsonFixer->fix($jsonImporter)->benchmark);
    $jsonExporter->commit;
    undef($jsonExporter);
    return ($jsonOut, $num);
}

sub marc2marc {
    my $sigel = shift;
    my $input = shift;

    my $output = 'output.mrc';
    print "converting $input to $output (MARC to MARC)\n";
    my $marcImporter  = Catmandu->importer('MARC', file => $input);
    my $marcFixer = Catmandu::Fix->new(
    #     variables => { sigel => $sigel},
    #     fixes => ['marc_add_field(912, {{sigel}})'],
        fixes => ['add_field(foo,bar)']
    );
    my $marcExporter = Catmandu->exporter('MARC', file => $output);
    my $fixed_marcImporter = $marcFixer->fix($marcImporter);
    $marcExporter->add_many($fixed_marcImporter->benchmark);
    # $marcExporter->add_many($marcImporter);

    $marcExporter->commit;
    undef($marcExporter);
    return $output;
}
